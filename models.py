from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from database import Base


class Metasource(Base):
    __tablename__ = 'Metasources'
    id = Column(Integer, primary_key=True)
    title = Column(String(30), nullable=False)
    url = Column(String(20), nullable=False)
    search_by = Column(String(20), nullable=True)
    search = Column(String(30), nullable=True)

    def __repr__(self):
        return '<{}: {}>'.format(self.__class__.__name__,
                                 {k: v for k, v in self.__dict__.items()
                                  if not k.startswith('_')}.values())

class Source(Base):
    __tablename__ = 'Sources'
    id = Column(Integer, primary_key=True)
    title = Column(String(30), nullable=False)
    category = Column(String(30), nullable=False)
    rss = Column(String(120), nullable=False)
    parent = Column(Integer, ForeignKey('Metasources.id'))
    Metasources = relationship(Metasource)

    def __repr__(self):
        return '<{}: {}>'.format(self.__class__.__name__,
                                 {k: v for k, v in self.__dict__.items()
                                  if not k.startswith('_')}.values())

class Entry(Base):
    __tablename__ = 'Entries'
    id = Column(Integer, primary_key=True)
    date = Column(String(20), nullable=False)
    title = Column(String(200), nullable=False)
    summary = Column(String(2000), nullable=True)  # nullable==True couse news body may be missing
    text = Column(String(10000), nullable=True)
    link = Column(String(200), nullable=False)
    processed = Column(Integer, nullable=False)
    source = Column(Integer, ForeignKey('Sources.id'))
    Sources = relationship(Source)

    def __repr__(self):
        return '<{}: {}>'.format(self.__class__.__name__,
                                 {k: v for k, v in self.__dict__.items()
                                  if not k.startswith('_')}.values())
